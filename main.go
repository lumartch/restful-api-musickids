package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	_ "github.com/go-sql-driver/mysql"
)

var usr string = "root"
var pass string = "123456789"
var ip string = "127.0.0.1"
var database string = "musickids"

type User struct {
	Correo    string
	Nickname  string
	Telefono  string
	Pass      string
	Avatar    string
	Nombre    string
	Pregunta  int
	Respuesta string
}

type LessonFailures struct {
	Titulo string
	Fallos int
}

type LessonFinished struct {
	Titulo    string
	Terminado bool
}

type LessonPUT struct {
	Correo string
	Field  string
	Value  int
}
type Song struct {
	Id        int
	Titulo    string
	Autor     string
	Duracion  string
	Path      string
	Vistas    int
	Favorita  int
	Terminada int
}
type SongPOSTPUT struct {
	Correo   string
	Song_id  int
	Favorito int
}

func NewSHA256(data string) string {
	hash := sha256.Sum256([]byte(data))
	return fmt.Sprintf("%x", hash)
}

func Songs(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")

	fmt.Println("Handling Songs...")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	switch req.Method {
	case "POST":
		fmt.Println("Updating finished song...")
		var song_p SongPOSTPUT
		err := json.NewDecoder(req.Body).Decode(&song_p)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		song_p.Correo = NewSHA256(song_p.Correo)

		selectDB, err := db.Query("SELECT COUNT(*) FROM usr_can WHERE usr_can.id_cancion = '" + fmt.Sprint(song_p.Song_id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "')")
		var count int64 = 0
		if err != nil {
			res.Write([]byte(`0`))
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		update_song, err := db.Query("UPDATE canciones SET vistas = vistas + 1 WHERE id = '" + fmt.Sprint(song_p.Song_id) + "'")
		if err != nil {
			res.Write([]byte(`0`))
			panic(err.Error())
		}
		defer update_song.Close()

		if count == 1 {
			sel_end, er := db.Query("SELECT terminada FROM usr_can WHERE usr_can.id_cancion = '" + fmt.Sprint(song_p.Song_id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "')")
			var terminada int64 = 0
			if er != nil {
				res.Write([]byte(`0`))
				panic(er.Error())
			}
			for sel_end.Next() {
				er = sel_end.Scan(&terminada)
				if er != nil {
					panic(er.Error())
				}
			}
			defer sel_end.Close()

			if terminada == 0 {
				update_user_can, err := db.Query("UPDATE usr_can SET terminada=1 WHERE usr_can.id_cancion='" + fmt.Sprint(song_p.Song_id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "')")
				if err != nil {
					res.Write([]byte(`0`))
					panic(err.Error())
				}
				defer update_user_can.Close()

				update_user, err := db.Query("UPDATE usuario SET can_terminadas = can_terminadas + 1 WHERE correo = '" + song_p.Correo + "'")
				if err != nil {
					res.Write([]byte(`0`))
					panic(err.Error())
				}
				defer update_user.Close()

				fmt.Println("Songs and user info updated!")
				res.Write([]byte(`1`))
			} else {
				res.Write([]byte(`1`))
			}
		} else {
			insert, err := db.Query("INSERT INTO usr_can (id, id_usuario, id_cancion, favorita, terminada) VALUES (NULL, (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "'), '" + fmt.Sprint(song_p.Song_id) + "', 0, 1)")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer insert.Close()

			update_user, err := db.Query("UPDATE usuario SET can_terminadas = can_terminadas + 1 WHERE correo = '" + song_p.Correo + "'")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer update_user.Close()

			fmt.Println("Songs info updated and user info inserted!")
			res.Write([]byte(`1`))
		}
	case "GET":
		fmt.Println("Building Songs Json...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" {
			fmt.Println("Url Param 'correo' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))
		search := params.Get("search")

		js := "{"
		selectDB, err := db.Query("SELECT * FROM canciones WHERE titulo LIKE '%" + search + "%' OR autor LIKE '%" + search + "%'")
		if err != nil {
			panic(err.Error())
		}
		for selectDB.Next() {
			var song Song
			err = selectDB.Scan(&song.Id, &song.Titulo, &song.Autor, &song.Duracion, &song.Path, &song.Vistas)
			if err != nil {
				panic(err.Error())
			}
			js = js + `
			"` + fmt.Sprint(song.Id) + `" : {
				"title": "` + song.Titulo + `",
				"author": "` + song.Autor + `",
				"duration": "` + song.Duracion + `",
				"path": "` + song.Path + `",
				"views": ` + fmt.Sprint(song.Vistas) + `,`
			sel, er := db.Query("SELECT count(*) FROM usr_can WHERE usr_can.id_cancion = '" + fmt.Sprint(song.Id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + correo + "')")
			var count int64
			if er != nil {
				panic(er.Error())
			}
			for sel.Next() {
				er = sel.Scan(&count)
				if er != nil {
					panic(err.Error())
				}
			}
			defer sel.Close()

			if count == 1 {
				s, e := db.Query("SELECT favorita, terminada FROM usr_can WHERE usr_can.id_cancion = '" + fmt.Sprint(song.Id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + correo + "')")
				if e != nil {
					panic(e.Error())
				}
				for s.Next() {
					e = s.Scan(&song.Favorita, &song.Terminada)
					if e != nil {
						panic(e.Error())
					}
				}
				defer sel.Close()
			}
			js = js + `
				"favorite": ` + fmt.Sprint(song.Favorita) + `,
				"finished": ` + fmt.Sprint(song.Terminada) + `
			},`
		}
		defer selectDB.Close()

		if js != `{` {
			js = js[:len(js)-1]
		}
		js = js + `}`
		fmt.Println("Delivering list of songs!")
		res.Write([]byte(js))

	case "PUT":
		fmt.Println("Updating favorite song...")
		var song_p SongPOSTPUT
		err := json.NewDecoder(req.Body).Decode(&song_p)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		song_p.Correo = NewSHA256(song_p.Correo)

		selectDB, err := db.Query("SELECT COUNT(*) FROM usr_can WHERE usr_can.id_cancion = '" + fmt.Sprint(song_p.Song_id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "')")
		var count int64 = 0
		if err != nil {
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if count == 0 {
			insert, err := db.Query("INSERT INTO usr_can (id, id_usuario, id_cancion, favorita, terminada) VALUES (NULL, (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "'), '" + fmt.Sprint(song_p.Song_id) + "', '" + fmt.Sprint(song_p.Favorito) + "', 0)")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer insert.Close()
			fmt.Println("New row inserted!")
			res.Write([]byte(`1`))
		} else {
			update, err := db.Query("UPDATE usr_can SET favorita='" + fmt.Sprint(song_p.Favorito) + "' WHERE usr_can.id_cancion='" + fmt.Sprint(song_p.Song_id) + "' AND usr_can.id_usuario = (SELECT id FROM usuario WHERE correo = '" + song_p.Correo + "')")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer update.Close()
			fmt.Println("Favorite song updated!")
			res.Write([]byte(`1`))
		}
	}
}

func Settings(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")

	fmt.Println("Handling Settings...")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	switch req.Method {
	case "GET":
		fmt.Println("Building Settings Json...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" {
			fmt.Println("Url Param 'correo' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))

		selectDB, err := db.Query("SELECT COUNT(*) FROM usuario WHERE correo='" + correo + "'")
		var count int64 = 0
		if err != nil {
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if count == 1 {
			var user User
			selectDB, err := db.Query("SELECT avatar, nickname, nombre, telefono FROM usuario WHERE correo='" + correo + "'")
			if err != nil {
				res.Write([]byte(`{}`))
				panic(err.Error())
			}
			for selectDB.Next() {
				selectDB.Scan(&user.Avatar, &user.Nickname, &user.Nombre, &user.Telefono)
			}
			defer selectDB.Close()

			js := `{
				"avatar": "` + user.Avatar + `",
				"nombre": "` + user.Nombre + `",
				"nickname": "` + user.Nickname + `",
				"telefono": "` + user.Telefono + `"
			}`

			fmt.Println("Settings: Delivering user's information!")
			res.Write([]byte(js))
			defer selectDB.Close()
		} else {
			fmt.Println("Settings: There's no '" + params.Get("correo") + "' in database!")
			res.Write([]byte(`{}`))
		}
	case "PUT":
		fmt.Println("Updating settings...")
		var user User
		err := json.NewDecoder(req.Body).Decode(&user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		user.Correo = NewSHA256(user.Correo)

		query := "UPDATE usuario SET nickname='" + user.Nickname + "', nombre='" + user.Nombre + "', telefono='" + user.Telefono + "', avatar='" + user.Avatar + "'"
		if user.Pass != "" {
			user.Pass = NewSHA256(user.Pass)
			query = query + ", password='" + user.Pass + "'"
		}
		query = query + " WHERE correo='" + user.Correo + "'"

		update, err := db.Query(query)
		if err != nil {
			res.Write([]byte(`0`))
			panic(err.Error())
		}
		defer update.Close()

		fmt.Println("Information updated!")
		res.Write([]byte(`1`))
	}
}

func Lessons(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")
	fmt.Println("Handling Lessons...")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	switch req.Method {
	case "GET":
		fmt.Println("Building Lessons Json...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" {
			fmt.Println("Url Param 'correo' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))

		selectDB, err := db.Query("SELECT lecciones.titulo FROM (index_lecciones INNER JOIN lecciones ON index_lecciones.id_leccion = lecciones.id INNER JOIN usuario ON index_lecciones.id_usuario = usuario.id) WHERE index_lecciones.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + correo + "')")
		ls := make(map[string]bool)
		if err != nil {
			panic(err.Error())
		} else {
			for selectDB.Next() {
				var l LessonFinished
				selectDB.Scan(&l.Titulo)
				ls[l.Titulo] = true
			}
		}
		defer selectDB.Close()

		js := `
		{
			"Presentación": {
				"Introducción":         ` + fmt.Sprint(ls["introduccion"]) + `,
				"Lecciones":            ` + fmt.Sprint(ls["que_es_musica"]) + `,
				"Biblioteca":  			` + fmt.Sprint(ls["musica_animo"]) + `,
				"Configuracion":        ` + fmt.Sprint(ls["helpybot"]) + `                
			},
			
			"Sonidos": {
				"Sonido largo":         ` + fmt.Sprint(ls["sonido_largo"]) + `,
				"Sonido corto":         ` + fmt.Sprint(ls["sonido_corto"]) + `,
				"Sonido agudo":         ` + fmt.Sprint(ls["sonido_agudo"]) + `,
				"Sonido grave":         ` + fmt.Sprint(ls["sonido_grave"]) + `,
				"Sonido fuerte":        ` + fmt.Sprint(ls["sonido_fuerte"]) + `,
				"Sonido suave":         ` + fmt.Sprint(ls["sonido_suave"]) + `,
				"Sonido consonante":    ` + fmt.Sprint(ls["sonido_consonante"]) + `,
				"Sonido disonante":     ` + fmt.Sprint(ls["sonido_disonante"]) + `,
				"Sonido hueco":         ` + fmt.Sprint(ls["sonido_hueco"]) + `
			},

			"Propiedades del sonido": {
				"Duración":             ` + fmt.Sprint(ls["duracion"]) + `,
				"Altura":               ` + fmt.Sprint(ls["altura"]) + `,
				"Volumen":              ` + fmt.Sprint(ls["volumen"]) + `,
				"Timbre":               ` + fmt.Sprint(ls["timbre"]) + `
			},

			"Las escalas": {
				"Las notas":            ` + fmt.Sprint(ls["notas"]) + `,
				"Intervalo 2da - Intro":` + fmt.Sprint(ls["2da_intro"]) + `,  
				"Escala mayor":         ` + fmt.Sprint(ls["escala_mayor"]) + `,
				"Escala menor":         ` + fmt.Sprint(ls["escala_menor"]) + `,
				"Escala cromática":     ` + fmt.Sprint(ls["escala_cromatica"]) + `
			},
			
			"Los intervalos": {
				"Intervalos":           ` + fmt.Sprint(ls["intervalos"]) + `,
				"Intervalo 2da":        ` + fmt.Sprint(ls["intervalos_2"]) + `,
				"Intervalo 3ra":        ` + fmt.Sprint(ls["intervalos_3"]) + `,
				"Intervalo 4ta":        ` + fmt.Sprint(ls["intervalos_4"]) + `,
				"Intervalo 5ta":        ` + fmt.Sprint(ls["intervalos_5"]) + `,
				"Intervalo 6ta":        ` + fmt.Sprint(ls["intervalos_6"]) + `,
				"Intervalo 7ma":        ` + fmt.Sprint(ls["intervalos_7"]) + `,
				"Intervalo 8va":        ` + fmt.Sprint(ls["intervalos_8"]) + `
			},
			
			"Los acordes": {
				"Acordes":              ` + fmt.Sprint(ls["acorde"]) + `,
				"Acorde Mayor":         ` + fmt.Sprint(ls["acorde_mayor"]) + `,
				"Acorde Menor":         ` + fmt.Sprint(ls["acorde_menor"]) + `,
				"Acorde disminuido":    ` + fmt.Sprint(ls["acorde_disminuido"]) + `,
				"Acorde aumentado":     ` + fmt.Sprint(ls["acorde_aumentado"]) + `
			},

			"Las figuras rítmicas": {
				"Figuras rítmicas":     ` + fmt.Sprint(ls["figuras"]) + `,
				"La redonda":           ` + fmt.Sprint(ls["redonda"]) + `,
				"La blanca":            ` + fmt.Sprint(ls["blanca"]) + `,
				"La negra":             ` + fmt.Sprint(ls["negra"]) + `,
				"La corchea":           ` + fmt.Sprint(ls["corchea"]) + `
			},

			"Leyendo partituras": {
				"La partitura":         ` + fmt.Sprint(ls["partitura"]) + `,
				"Pentagrama":           ` + fmt.Sprint(ls["pentagrama"]) + `,
				"Claves":               ` + fmt.Sprint(ls["claves"]) + `,
				"Armaduras":            ` + fmt.Sprint(ls["armaduras"]) + `,
				"Indicadores":          ` + fmt.Sprint(ls["indicadores"]) + `
			}
		}`
		fmt.Println("Delivering Lessons Json!")
		res.Write([]byte(js))
	case "PUT":
		fmt.Println("Searching for Lesson row...")
		var ls LessonPUT
		err := json.NewDecoder(req.Body).Decode(&ls)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		ls.Correo = NewSHA256(ls.Correo)

		selectDB, err := db.Query("SELECT COUNT(*) FROM (index_lecciones INNER JOIN lecciones ON index_lecciones.id_leccion = lecciones.id INNER JOIN usuario ON index_lecciones.id_usuario = usuario.id) WHERE index_lecciones.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "') AND lecciones.titulo = '" + ls.Field + "'")
		var count int64 = 0
		if err != nil {
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if count == 0 {
			insert, err := db.Query("INSERT INTO index_lecciones (id, id_usuario, id_leccion) VALUES (NULL, (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "'), (SELECT id FROM lecciones WHERE lecciones.titulo = '" + ls.Field + "'))")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer insert.Close()

			update, err := db.Query("UPDATE usuario SET lec_terminadas = lec_terminadas + 1 WHERE correo = '" + ls.Correo + "'")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer update.Close()

			fmt.Println("Lesson Inserted and user updated!")
			res.Write([]byte(`1`))
		} else {
			fmt.Println("Lesson already inserted!")
			res.Write([]byte(`0`))
		}
	}
}

func IA(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")

	fmt.Println("Handling IA Json...")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	switch req.Method {
	case "GET":
		fmt.Println("Building IA Json...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" {
			fmt.Println("Url Param 'correo' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))

		selectDB, err := db.Query("SELECT lecciones.titulo, index_ia.fallos FROM (index_ia INNER JOIN lecciones ON index_ia.id_leccion = lecciones.id INNER JOIN usuario ON index_ia.id_usuario = usuario.id) WHERE index_ia.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + correo + "')")
		ls := make(map[string]int)
		if err != nil {
			panic(err.Error())
		} else {
			for selectDB.Next() {
				var l LessonFailures
				selectDB.Scan(&l.Titulo, &l.Fallos)
				ls[l.Titulo] = l.Fallos
			}
		}
		defer selectDB.Close()

		js := `
        {
            "Sonidos": {
                "Sonido largo": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_largo"]) + `
                },
                "Sonido corto": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_corto"]) + `
                },
                "Sonido agudo": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_agudo"]) + `
                },
                "Sonido grave": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_grave"]) + `
                },
                "Sonido consonante": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_consonante"]) + `
                },
                "Sonido disonante": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_disonante"]) + `
                },
                "Sonido hueco": {
                    "Fallos": ` + fmt.Sprint(ls["sonido_hueco"]) + `
                }
            }, 
        
            "Las escalas": {  
                "Intervalo 2da - Intro": {
                    "Fallos": 2
                },  
                "Escala mayor": {
                    "Fallos": ` + fmt.Sprint(ls["escala_mayor"]) + `
                },
                "Escala menor": {
                    "Fallos": ` + fmt.Sprint(ls["escala_menor"]) + `
                },
                "Escala cromática": {
                    "Fallos": ` + fmt.Sprint(ls["escala_cromatica"]) + `
                }
            },
        
            "Los intervalos": {
                "Intervalo 2da": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_2"]) + `
                },
                "Intervalo 3ra": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_3"]) + `
                },
                "Intervalo 4ta": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_4"]) + `
                },
                "Intervalo 5ta": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_5"]) + `
                },
                "Intervalo 6ta": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_6"]) + `
                },
                "Intervalo 7ma": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_7"]) + `
                },
                "Intervalo 8va": {
                    "Fallos": ` + fmt.Sprint(ls["intervalos_8"]) + `
                }
            },
            
            "Los acordes": {
                "Acorde Mayor": {
                    "Fallos": ` + fmt.Sprint(ls["acorde_mayor"]) + `
                },
                "Acorde Menor": {
                    "Fallos": ` + fmt.Sprint(ls["acorde_menor"]) + `
                },
                "Acorde disminuido": {
                    "Fallos": ` + fmt.Sprint(ls["acorde_disminuido"]) + `
                },
                "Acorde aumentado": {
                    "Fallos": ` + fmt.Sprint(ls["acorde_aumentado"]) + `
                }
            }
        }`
		fmt.Println("Delivering IA Json!")
		res.Write([]byte(js))
	case "PUT":
		fmt.Println("Searching for IA Lesson row...")
		var ls LessonPUT
		err := json.NewDecoder(req.Body).Decode(&ls)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		ls.Correo = NewSHA256(ls.Correo)

		selectDB, err := db.Query("SELECT COUNT(*) FROM (index_ia INNER JOIN lecciones ON index_ia.id_leccion = lecciones.id INNER JOIN usuario ON index_ia.id_usuario = usuario.id) WHERE index_ia.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "') AND lecciones.titulo = '" + ls.Field + "'")
		var count int64 = 0
		if err != nil {
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if count == 0 {
			if ls.Value == -1 {
				ls.Value = 0
			} else {
				ls.Value = 1
			}
			insert, err := db.Query("INSERT INTO index_ia (id, id_usuario, id_leccion, fallos) VALUES (NULL, (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "'), (SELECT id FROM lecciones WHERE lecciones.titulo = '" + ls.Field + "'), '" + fmt.Sprint(ls.Value) + "')")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer insert.Close()
			fmt.Println("IA Lesson Inserted!")
			res.Write([]byte(`1`))
		} else {
			var query string
			if ls.Value == 1 {
				query = "UPDATE index_ia SET fallos = fallos + 1 WHERE (index_ia.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "') AND index_ia.id_leccion = (SELECT id FROM lecciones WHERE lecciones.titulo = '" + ls.Field + "'))"
			} else {
				query = "UPDATE index_ia SET fallos = fallos - 1 WHERE (index_ia.id_usuario = (SELECT id FROM usuario WHERE usuario.correo = '" + ls.Correo + "') AND index_ia.id_leccion = (SELECT id FROM lecciones WHERE lecciones.titulo = '" + ls.Field + "')) AND fallos > 0"
			}
			update, err := db.Query(query)
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer update.Close()
			fmt.Println("IA Lesson Updated!")
			res.Write([]byte(`1`))
		}
	}
}

func Account(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Handling Account...")
	defer db.Close()

	switch req.Method {
	case "POST":
		fmt.Println("Creating new account...")
		var user User
		err := json.NewDecoder(req.Body).Decode(&user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		user.Correo = NewSHA256(user.Correo)
		user.Pass = NewSHA256(user.Pass)
		user.Respuesta = NewSHA256(user.Respuesta)

		selectDB, err := db.Query("SELECT count(*) FROM usuario WHERE correo='" + user.Correo + "'")
		var count int64 = 0
		if err != nil {
			panic(err.Error())
		} else {
			for selectDB.Next() {
				selectDB.Scan(&count)
			}
		}
		defer selectDB.Close()
		if count == 0 && user.Nickname != "" {
			insert, err := db.Query("INSERT INTO usuario (correo, nickname, telefono, password, avatar, nombre, pregunta, respuesta) VALUES ('" + user.Correo + "', '" + user.Nickname + "', '" + user.Telefono + "', '" + user.Pass + "', '" + user.Avatar + "', '" + user.Nombre + "', '" + fmt.Sprint(user.Pregunta) + "', '" + user.Respuesta + "')")
			if err != nil {
				res.Write([]byte(`0`))
				panic(err.Error())
			}
			defer insert.Close()
			fmt.Println("New account created!")
			res.Write([]byte(`1`))
		} else {
			fmt.Println("Unable to create account... Email already exist in data base!")
			res.Write([]byte(`0`))
		}
	case "GET":
		fmt.Println("Getting email...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" || params.Get("recover") == "" {
			fmt.Println("Url Param 'correo' or 'recover' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))
		recover := params.Get("recover")

		selectDB, err := db.Query("SELECT count(*) FROM usuario WHERE correo='" + correo + "'")
		var count int64 = 0
		if err != nil {
			res.Write([]byte(`0`))
			panic(err.Error())
		}
		for selectDB.Next() {
			err = selectDB.Scan(&count)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if recover == "false" {
			if count == 0 {
				fmt.Println("The email is available!")
				res.Write([]byte(`1`))
			} else {
				fmt.Println("The email is not available!")
				res.Write([]byte(`0`))
			}
		} else {
			if count == 1 {
				selectDB, err := db.Query("SELECT pregunta FROM usuario WHERE correo='" + correo + "'")
				if err != nil {
					res.Write([]byte(`undefined`))
					panic(err.Error())
				}
				var question string
				for selectDB.Next() {
					err = selectDB.Scan(&question)
					if err != nil {
						panic(err.Error())
					}
				}
				fmt.Println("Question Index: ", question)
				res.Write([]byte(question))
				defer selectDB.Close()
			} else {
				fmt.Println("The email is not inside of the database!")
				res.Write([]byte(`undefined`))
			}
		}

	case "PUT":
		fmt.Println("Updating password...")
		var user User
		err := json.NewDecoder(req.Body).Decode(&user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		user.Correo = NewSHA256(user.Correo)
		user.Pass = NewSHA256(user.Pass)
		user.Respuesta = NewSHA256(user.Respuesta)

		update, err := db.Query("UPDATE usuario SET password = '" + user.Pass + "' WHERE correo='" + user.Correo + "' and respuesta='" + user.Respuesta + "'")
		if err != nil {
			panic(err.Error())
		}
		defer update.Close()

		selectDB, err := db.Query("SELECT password FROM usuario WHERE correo='" + user.Correo + "'")
		if err != nil {
			res.Write([]byte(`0`))
			panic(err.Error())
		}
		var pass string
		for selectDB.Next() {
			err = selectDB.Scan(&pass)
			if err != nil {
				panic(err.Error())
			}
		}
		defer selectDB.Close()

		if pass == user.Pass {
			fmt.Println("Password updated!!")
			res.Write([]byte(`1`))
		} else {
			fmt.Println("The password was not updated.")
			res.Write([]byte(`0`))
		}
	}
}

func Login(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "*")
	res.Header().Set("Access-Control-Allow-Methods", "*")
	res.Header().Set("Content-Type", "application/json")

	fmt.Println("Handling Login...")
	db, err := sql.Open("mysql", usr+":"+pass+"@tcp("+ip+")/"+database)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	switch req.Method {
	case "POST":
		var user User
		err := json.NewDecoder(req.Body).Decode(&user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		user.Correo = NewSHA256(user.Correo)
		user.Pass = NewSHA256(user.Pass)

		selectDB, err := db.Query("SELECT count(*) FROM usuario WHERE correo='" + user.Correo + "' AND password ='" + user.Pass + "'")
		var count int64 = 0
		if err != nil {
			res.Write([]byte(`{}`))
			panic(err.Error())
		}
		for selectDB.Next() {
			selectDB.Scan(&count)
		}
		defer selectDB.Close()

		if count == 1 {
			selectDB, err := db.Query("SELECT avatar, nickname, nombre, telefono FROM usuario WHERE correo='" + user.Correo + "' AND password ='" + user.Pass + "'")
			if err != nil {
				res.Write([]byte(`{}`))
				panic(err.Error())
			}
			for selectDB.Next() {
				selectDB.Scan(&user.Avatar, &user.Nickname, &user.Nombre, &user.Telefono)
			}
			defer selectDB.Close()
			js := `{
				"avatar": "` + user.Avatar + `",
				"nickname": "` + user.Nickname + `",
				"name": "` + user.Nombre + `",
				"phone": "` + user.Telefono + `"
			}`
			fmt.Println("User is Loggin!")
			res.Write([]byte(js))
		} else {
			fmt.Println("There was an error on the data for Loggin...")
			res.Write([]byte(`{}`))
		}
	case "GET":
		fmt.Println("Getting user info...")
		u, _ := url.Parse(req.URL.String())
		params := u.Query()

		if params.Get("correo") == "" {
			fmt.Println("Url Param 'correo' is missing")
			return
		}
		correo := NewSHA256(params.Get("correo"))

		selectDB, err := db.Query("SELECT count(*) FROM usuario WHERE correo='" + correo + "'")
		var count int64 = 0
		if err != nil {
			res.Write([]byte(`{}`))
			panic(err.Error())
		}
		for selectDB.Next() {
			selectDB.Scan(&count)
		}
		defer selectDB.Close()

		if count == 1 {
			var user User
			selectDB, err := db.Query("SELECT avatar, nickname, nombre, telefono FROM usuario WHERE correo='" + correo + "'")
			if err != nil {
				res.Write([]byte(`{}`))
				panic(err.Error())
			}
			for selectDB.Next() {
				selectDB.Scan(&user.Avatar, &user.Nickname, &user.Nombre, &user.Telefono)
			}
			defer selectDB.Close()
			js := `{
				"avatar": "` + user.Avatar + `",
				"nickname": "` + user.Nickname + `",
				"name": "` + user.Nombre + `",
				"phone": "` + user.Telefono + `"
			}`
			fmt.Println("Delivering user info!")
			res.Write([]byte(js))
		} else {
			fmt.Println("The user does not exist in the database!")
			res.Write([]byte(`{}`))
		}
	}
}

func main() {
	// export GO111MODULE=auto
	http.HandleFunc("/account/", Account)
	http.HandleFunc("/ia/", IA)
	http.HandleFunc("/lessons/", Lessons)
	http.HandleFunc("/login/", Login)
	http.HandleFunc("/settings/", Settings)
	http.HandleFunc("/songs/", Songs)
	fmt.Println("RUNNING Musickids RESTful API...")
	http.ListenAndServe("192.168.100.25:8080", nil)
}
